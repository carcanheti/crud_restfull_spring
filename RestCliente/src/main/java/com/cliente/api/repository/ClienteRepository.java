package com.cliente.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cliente.api.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, String>{

}
