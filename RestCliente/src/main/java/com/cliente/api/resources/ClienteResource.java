package com.cliente.api.resources;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cliente.api.model.Cliente;
import com.cliente.api.repository.ClienteRepository;

@RestController
@RequestMapping("/cliente")
public class ClienteResource {

	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping(produces = "application/json")
	public @ResponseBody Iterable<Cliente> listaClientes() {
		Iterable<Cliente> listaClientes = clienteRepository.findAll();
		return listaClientes;
	}
	
	@GetMapping("/cliente/{id}")
	public Cliente retrieveCliente(@PathVariable long id) throws Exception {
		Optional<Cliente> cliente = clienteRepository.findById(String.valueOf(id));
		
		if (!cliente.isPresent())
			throw new Exception("id-" + id);

		return cliente.get();
	}

	@PostMapping()
	public Cliente cadastraCliente(@RequestBody @Valid Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@DeleteMapping()
	public Cliente deletaCliente(@RequestBody Cliente cliente) {
		clienteRepository.delete(cliente);
		return cliente;
	}
}
